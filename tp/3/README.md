# TP3 : Your own shiet

- [TP3 : Your own shiet](#tp3--your-own-shiet)
- [Intro](#intro)
- [Contenu attendu](#contenu-attendu)
- [Barême](#barême)

# Intro

**Dans ce TP, vous allez installer une solution de votre choix.** Pas la développer, mais bien installer et configurer un truc existant.

Pensez large :

- hébergement de fichiers
- streaming audio/vidéo
- webradio
- héberger votre propre dépôt git
- serveur de jeu
- serveur VPN
- etc.

La solution doit être un projet libre et open-source (souvent, le code et la doc seront accessibles sur GitHub).

**Vous devrez me consulter pour que je valide le choix de la solution.**

![Use Linux](./pics/use_linux.jpg)

# Contenu attendu

➜ **Vous devrez documenter dans le rendu :**

- installation
- configuration
- maintien en condition opérationnelle

➜ **Pour ce qui est du maintien en condition opérationnelle**,  reposez-vous sur ce qu'on a vu au TP précédent :

- sauvegarde de fichiers
- sauvegarde de base de données
- monitoring
- une doc correct, ça en fait partie, donc un compte-rendu clean dans votre cas ici

➜ **Pensez aux bonnes pratiques** :

- un service = une machine
- principe du moindre privilèges
  - firewall
  - un utilisateur par application
  - permissions sur les fichiers
- la checklist habituelle (adressage IP, nommage des machines, etc.)

➜ **Petite nouveauté : l'automatisation** :

- vous devrez rédiger un script qui automatisera le déploiement de la solution
- pour installer le truc que vous avez choisi, plutôt que de lire et suivre la doc, on lance juste votre script et il fait tout

# Barême

Les points que je noterai sont les suivants :

➜ **Solution fonctionnelle /4**

- si ça full marche, ça fait 4 points

➜ **Respect des bonnes pratiques /4**

- firewall actif
- moindres privilège
- bonne gestion des droits

➜ **Automatisation de la solution /4**

- un script `bash` qui carry
- je noterai le fait qu'il marche, tout autant le fait qu'il soit lisible et compréhensible

➜ **Qualité de la doc /4**

- une bonne solution sans une bonne doc **c'est d'la merde**
- habituez-vous à rédiger des documents propres

➜ **Maintien en condition opérationnelle /4**

- backup, monitoring, etc.
- tout est bon à prendre pour avoir quelque chose qui dure dans le temps

**Ces 5 choses sont cruciales.** Savoir réaliser ces 5 choses font de vous un utilisateur avancé de Linux, plus particulièrement, d'un point de vue admin.

> Si vous savez faire ces 5 choses, vous savez faire mon job :)