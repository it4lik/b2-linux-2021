# TP2 pt. 1 : Gestion de service

# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

🌞 **Installer le serveur Apache**

```bash
[it4@web ~]$ sudo dnf install -y httpd
```

🌞 **Démarrer le service Apache**

```bash
# Démarrage
[it4@web ~]$ sudo systemctl start httpd

# Lancement au boot
[it4@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

# Sur quel port tourne Apache ?
[it4@web ~]$ sudo ss -alnpt | grep httpd
LISTEN 0      128                *:80              *:*    users:(("httpd",pid=24458,fd=4),("httpd",pid=24457,fd=4),("httpd",pid=24456,fd=4),("httpd",pid=24453,fd=4))

# Firewall conf
[it4@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[it4@web ~]$ sudo firewall-cmd --reload
success

# Firewall state
[it4@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports: 80/tcp 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 **TEST**

```bash
# Service activé
[it4@web ~]$ sudo systemctl is-active httpd
active

# Activé au boot
[it4@web ~]$ sudo systemctl is-enabled httpd
enabled

# Test local
[it4@web ~]$ curl localhost
<html>
[...]
```

Sur l'hôte

```bash
[it4@nowhere ~]$ curl 10.102.1.11
<html>
```
## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

```bash
[it4@web ~]$ sudo systemctl cat httpd --no-pager
# /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#	[Service]
#	Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

```bash
# Process httpd sous l'identité apache
[it4@web ~]$ ps -ef | grep -i httpd
root       23891       1  0 11:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     23892   23891  0 11:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     23893   23891  0 11:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     23894   23891  0 11:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     23895   23891  0 11:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
it4        24238   23858  0 11:05 pts/0    00:00:00 grep --color=auto -i httpd

# Page d'accueil 
[it4@web ~]$ ls -al /usr/share/httpd/noindex/index.html
lrwxrwxrwx. 1 root root 25 Oct 13 04:30 /usr/share/httpd/noindex/index.html -> ../../testpage/index.html

[it4@web ~]$ ls -al /usr/share/testpage/index.html
-rw-r--r--. 1 root root 7621 Jun 11 17:23 /usr/share/testpage/index.html

# Le fichier est bien accessible en lecture par l'utilisateur "apache"
```

🌞 **Changer l'utilisateur utilisé par Apache**

```bash
# voyons les paramètres de l'utiliateur apache par défaut
[it4@web ~]$ cat /etc/passwd | grep apache
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin

# création du nouveau user
[it4@web ~]$ sudo useradd web -m -d /usr/share/httpd/ -s /sbin/nologin
useradd: warning: the home directory already exists.
Not copying any file from skel directory into it.
Creating mailbox file: File exists

# modifier l'utilisateur qui lance Apache
[it4@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
[it4@web ~]$ cat /etc/httpd/conf/httpd.conf | grep "User web"
User web
[it4@web ~]$ cat /etc/httpd/conf/httpd.conf | grep "Group web"
Group web

# redémarrage d'Apache
[it4@web ~]$ sudo systemctl restart httpd

# vérification
[it4@web ~]$ ps -ef | grep -i httpd
root       24453       1  2 11:12 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web        24455   24453  0 11:12 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web        24456   24453  0 11:12 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web        24457   24453  0 11:12 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web        24458   24453  0 11:12 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
it4        24671   23858  0 11:12 pts/0    00:00:00 grep --color=auto -i httpd
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

```bash
# Modif de la conf Apache
[it4@web httpd]$ sudo vim /etc/httpd/conf/httpd.conf 
[it4@web httpd]$ 
[it4@web httpd]$ cat /etc/httpd/conf/httpd.conf | grep 8888
Listen 8888

# Redémarrage Apache
[it4@web httpd]$ sudo systemctl restart httpd

# Vérif
[it4@web httpd]$ sudo ss -alnpt | grep httpd
LISTEN 0      128                *:8888            *:*    users:(("httpd",pid=24918,fd=4),("httpd",pid=24917,fd=4),("httpd",pid=24916,fd=4),("httpd",pid=24913,fd=4))

# Conf firewall
[it4@web httpd]$ sudo firewall-cmd --permanent --remove-port=80/tcp
success
[it4@web httpd]$ sudo firewall-cmd --permanent --add-port=8888/tcp
success
[it4@web httpd]$ sudo firewall-cmd --reload
success

# Test local
[it4@web httpd]$ curl localhost:8888
<html>
[...]
```

Depuis l'hôte

```bash
[it4@nowhere ~]$ curl 10.102.1.11:8888 
```

📁 **Fichier `/etc/httpd/conf/httpd.conf`** 

# II. Une stack web plus avancée

## 2. Setup

🖥️ **VM db.tp2.linux**


### A. Serveur Web et NextCloud

🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`

```bash
# Install

# Conf apache
[it4@web ~]$ sudo mkdir /etc/httpd/sites-{enabled,available}

[it4@web ~]$ sudo vim /etc/httpd/sites-available/web.tp2.linux

[it4@web ~]$ sudo cat /etc/httpd/sites-available/web.tp2.linux
<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/web.tp2.linux/html/
  ServerName  web.tp2.linux

  <Directory /var/www/sub-domains/web.tp2.linux/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

[it4@web ~]$ sudo ln -s /etc/httpd/sites-available/web.tp2.linux /etc/httpd/sites-enabled/

# Préparation de la racine où se trouvera NextCloud
[it4@web ~]$ sudo mkdir -p /var/www/sub-domains/web.tp2.linux/html

# Config de PHP
## Récupération du timezone actuel (fuseau horaire)
[it4@web zoneinfo]$ timedatectl | grep zone
                Time zone: Europe/Paris (CEST, +0200)
## Config de PHP en accord
[it4@web zoneinfo]$ sudo vim /etc/opt/remi/php74/php.ini
[it4@web zoneinfo]$ cat /etc/opt/remi/php74/php.ini | grep timezone
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
date.timezone = "Europe/Paris"

# Récupération de NextCloud
[it4@web nextcloud]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
[...]
[it4@web nextcloud]$ unzip nextcloud-22.2.0.zip
[...]

# Déplacement de NextCloud dans le dossier servi par Apache
[it4@web nextcloud]$ cd nextcloud/
[it4@web nextcloud]$ sudo cp -Rf * /var/www/sub-domains/web.tp2.linux/html/
[...]
[it4@web nextcloud]$ sudo chown -Rf apache.apache /var/www/sub-domains/web.tp2.linux/

# On redémarre Apache (car on a modifié des fichiers dans /etc/httpd/)
[it4@web nextcloud]$ sudo systemctl restart httpd
```

📁 **Fichier `/etc/httpd/conf/httpd.conf`** (que vous pouvez renommer si besoin, vu que c'est le même nom que le dernier fichier demandé)
📁 **Fichier `/etc/httpd/sites-available/web.tp2.linux`**

### B. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

```bash
# Install
[it4@db ~]$ sudo dnf install -y mariadb-server
[...]

# Démarrage du service, maintenant et au boot
[it4@db ~]$ sudo systemctl enable --now mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

# Repérage du port qu'utilise mariadb (process "mysqld")
[it4@db ~]$ sudo ss -alnpt | grep mysql
LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=26339,fd=21))

# Ouverture du port
[it4@db ~]$ sudo firewall-cmd --permanent --add-port=3306/tcp
success
[it4@db ~]$ sudo firewall-cmd --reload
success
[it4@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports: 22/tcp 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```


Utilisation de `mysql_secure_installation` sur la machine `db.tp2.linux`. Cela préconfigure la base mariadb en augmentant son niveau de sécurité :

```bash
[it4@db ~]$ mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] Y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] Y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] Y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

🌞 **Préparation de la base pour NextCloud**

```bash
[it4@db ~]$ mysql -hlocalhost -uroot -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 20
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> Bye
```

🌞 **Exploration de la base de données**

```bash
[it4@web nextcloud]$ mysql -h10.102.1.12 -unextcloud -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 22
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.002 sec)

MariaDB [(none)]> use nextcloud;
Database changed
MariaDB [nextcloud]> show tables;
Empty set (0.002 sec)
```

```sql
SHOW DATABASES;
USE <DATABASE_NAME>;
SHOW TABLES;
```

La commande qui permet de lister tous les utilisateurs de la base de données :

```bash
[it4@db ~]$ mysql -hlocalhost -uroot -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 23
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> select user,host from mysql.user;
+-----------+-------------+
| user      | host        |
+-----------+-------------+
| nextcloud | 10.102.1.11 |
| root      | 127.0.0.1   |
| root      | ::1         |
| root      | localhost   |
+-----------+-------------+
4 rows in set (0.001 sec)
```

### C. Finaliser l'installation de NextCloud

🌞 sur votre PC

```bash
[it4@nowhere ~]$ sudo vim /etc/hosts
[it4@nowhere ~]$ cat /etc/hosts | grep web.tp2.linux
10.102.1.11 web.tp2.linux

[it4@nowhere ~]$ curl web.tp2.linux
<!DOCTYPE html>
<html>
<head>
	<script> window.location.href="index.php"; </script>
	<meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>
```

🌞 **Exploration de la base de données**

```bash
[it4@web nextcloud]$ mysql -h10.102.1.12 -unextcloud -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 30
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> use nextcloud;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [nextcloud]> show tables;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
[...]
| oc_users                    |
| oc_vcategory                |
| oc_vcategory_to_object      |
| oc_webauthn                 |
| oc_whats_new                |
+-----------------------------+
87 rows in set (0.001 sec)

MariaDB [nextcloud]> SELECT FOUND_ROWS();
+--------------+
| FOUND_ROWS() |
+--------------+
|           87 |
+--------------+
1 row in set (0.003 sec)
```

| Machine         | IP            | Service                 | Port ouvert     | IP autorisées |
|-----------------|---------------|-------------------------|-----------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 22/tcp 3306/tcp | all           |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 22/tcp 80/tcp   | all           |

Fichiers :

- [`/etc/httpd/conf/httpd.conf`](./files/httpd.conf)
- [`/etc/httpd/sites-available/web.tp2.linux`](./files/web.tp2.linux)
