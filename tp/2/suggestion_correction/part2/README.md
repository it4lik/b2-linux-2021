# TP2 pt. 2 : Maintien en condition opérationnelle

# Sommaire

- [TP2 pt. 2 : Maintien en condition opérationnelle](#tp2-pt-2--maintien-en-condition-opérationnelle)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
- [I. Monitoring](#i-monitoring)
  - [2. Setup](#2-setup)
- [II. Backup](#ii-backup)
  - [2. Partage NFS](#2-partage-nfs)
- [III. Reverse Proxy](#iii-reverse-proxy)
  - [2. Setup simple](#2-setup-simple)
  - [3. Bonus HTTPS](#3-bonus-https)
- [IV. Firewalling](#iv-firewalling)
  - [2. Mise en place](#2-mise-en-place)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web](#b-serveur-web)
    - [C. Serveur de backup](#c-serveur-de-backup)
    - [D. Reverse Proxy](#d-reverse-proxy)
    - [E. Tableau récap](#e-tableau-récap)

# 0. Prérequis

# I. Monitoring

## 2. Setup

🌞 **Manipulation du *service* Netdata**

```bash
# Inspection du service netdata
[it4@web ~]$ sudo systemctl is-active netdata
active
[it4@web ~]$ sudo systemctl is-enabled netdata
enabled

[it4@web ~]$ sudo ss -alnpt | grep netdata
LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=67049,fd=59))
LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=67049,fd=5))
LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=67049,fd=58))
LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=67049,fd=6))

# Ouverture du port firewall pour la WebUI
[it4@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[it4@web ~]$ sudo firewall-cmd --reload
success
```

🌞 **Setup Alerting**

```bash
[it4@web ~]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
Copying '/opt/netdata/usr/lib/netdata/conf.d/health_alarm_notify.conf' to '/opt/netdata/etc/netdata/health_alarm_notify.conf' ... 
Editing '/opt/netdata/etc/netdata/health_alarm_notify.conf' ...

[it4@web ~]$ cat /opt/netdata/etc/netdata/health_alarm_notify.conf | grep DISCORD
SEND_DISCORD="YES"
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/903953845992980480/jDFP5M9-uydhQrAy831QEYHaMsnBbBY_CcT_hyegLDHzTjtgKcXtPRIDoMhzC3hn62Kx"
DEFAULT_RECIPIENT_DISCORD="general"
[...]

[it4@web ~]$ sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
```

🌞 **Config alerting**

```bash
[it4@web netdata]$ cat /opt/netdata/etc/netdata/health.d/ram-tp2.conf 
 alarm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 40
  crit: $this > 50
  info: The percentage of RAM being used by the system.
```

# II. Backup

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?             |

🖥️ **VM `backup.tp2.linux`**

**Déroulez la [📝**checklist**📝](#checklist) sur cette VM.**

## 2. Partage NFS

🌞 **Setup environnement**

```bash
[it4@backup ~]$ sudo mkdir -p /srv/backups/web.tp2.linux/
```

🌞 **Setup partage NFS**

```bash
# Conf du serveur
[it4@backup ~]$ sudo vim /etc/idmapd.conf
[it4@backup ~]$ cat /etc/idmapd.conf | grep tp2.linux
Domain = tp2.linux
[it4@backup ~]$ sudo vim /etc/exports
[it4@backup ~]$ cat /etc/exports
/srv/backups/web.tp2.linux 10.102.1.11(rw,no_root_squash)

# Démarrage du service
[it4@backup ~]$ sudo systemctl start nfs-server
[it4@backup ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.

# Conf firewall
[it4@backup ~]$ sudo firewall-cmd --permanent --add-service=nfs
success
[it4@backup ~]$ sudo firewall-cmd --reload
success
[it4@backup ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: nfs
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 **Setup points de montage sur `web.tp2.linux`**

```bash
# Config du client
[it4@web]$ sudo dnf -y install nfs-utils
[...]
[it4@web]$ sudo vim /etc/idmapd.conf
[it4@web]$ cat /etc/idmapd.conf | grep tp2
Domain = tp2.linux

# Montage du partage NFS
[it4@web]$ sudo mount -t nfs backup.tp2.linux:/srv/backups/web.tp2.linux /srv/backup/

# Vérif
[it4@web]$ df -h | grep /srv/backup
backup.tp2.linux:/srv/backups/web.tp2.linux  6.2G  2.0G  4.3G  32% /srv/backup

[it4@web]$ mount | grep /srv/backup
backup.tp2.linux:/srv/backups/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)

# Test
[it4@web]$ sudo touch /srv/backup/meo
```

Faites en sorte que cette partition se monte automatiquement grâce au fichier `/etc/fstab`

```bash
# Modification du fstab
[it4@web]$ sudo vim /etc/fstab
[it4@web]$ sudo cat /etc/fstab | grep /srv/backup
backup.tp2.linux:/srv/backups/web.tp2.linux /srv/backup/ nfs defaults 0 0

# Test de lecture du fstab
[it4@web]$ sudo umount /srv/backup
[it4@web]$ sudo mount -a -v
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount.nfs: timeout set for Sat Oct 30 13:08:13 2021
mount.nfs: trying text-based options 'vers=4.2,addr=10.102.1.13,clientaddr=10.102.1.11'
/srv/backup              : successfully mounted
```

# III. Reverse Proxy

## 2. Setup simple

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?             |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | ?           | ?             |

🖥️ **VM `front.tp2.linu`x**

**Déroulez la [📝**checklist**📝](#checklist) sur cette VM.**

🌞 **Installer NGINX**

```bash
[it4@front ~]$ sudo dnf install -y epel-release && sudo dnf install -y nginx
[...]
```

🌞 **Tester !**

```bash
# Démarrage de NGINX, maintenant et au boot de la machine
[it4@front ~]$ sudo systemctl enable --now nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.

# Repérage du pot utilisé par NGINX
[it4@front ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=25868,fd=8),("nginx",pid=25867,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=25868,fd=9),("nginx",pid=25867,fd=9))

# Conf firewall
[it4@front ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success
[it4@front ~]$ sudo firewall-cmd --reload
success
[it4@front ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: 
  ports: 22/tcp 80/tcp
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
```

Test depuis l'hôte :

```bash
[it4@nowhere ~]$ curl 10.102.1.14
<html>
[...]
```

🌞 **Explorer la conf par défaut de NGINX**

```bash
# Repérage de l'utilisateur utilisé par NGINX
[it4@front ~]$ cat /etc/nginx/nginx.conf | grep user
user nginx;
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

# Repérage de la clause 'server' utilisée par défaut
[it4@front ~]$ cat /etc/nginx/nginx.conf | grep -A 20 'server {'
    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
[...]

# Repérage des include depuis le fichier de conf principal
[it4@front ~]$ cat /etc/nginx/nginx.conf | grep -i include
include /usr/share/nginx/modules/*.conf;
    include             /etc/nginx/mime.types;
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/default.d/*.conf;
#        include /etc/nginx/default.d/*.conf;
```

🌞 **Modifier la conf de NGINX**

- pour que ça fonctionne, le fichier `/etc/hosts` de la machine **DOIT** être rempli correctement, conformément à la **[📝**checklist**📝](#checklist)**
- supprimer le bloc `server {}` par défaut, pour ne plus présenter la page d'accueil NGINX
- créer un fichier `/etc/nginx/conf.d/web.tp2.linux.conf` avec le contenu suivant :
  - j'ai sur-commenté pour vous expliquer les lignes, n'hésitez pas à dégommer mes lignes de commentaires

```bash
[it4@localhost nginx]$ cat conf.d/web.tp2.linux.conf 
server {
    # on demande à NGINX d'écouter sur le port 80 pour notre NextCloud
    listen 80;

    # ici, c'est le nom de domaine utilisé pour joindre l'application
    # ce n'est pas le nom du reverse proxy, mais le nom que les clients devront saisir pour atteindre le site
    server_name web.tp3.linux; # ici, c'est le nom de domaine utilisé pour joindre l'application (pas forcéme

    # on définit un comportement quand la personne visite la racine du site (http://web.tp2.linux/)
    location / {
        # on renvoie tout le trafic vers la machine web.tp2.linux
        proxy_pass http://web.tp2.linux;
    }
}
```

## 3. Bonus HTTPS

```bash
[it4@front ~]$ cd ~

[it4@front ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
[...]
Common Name (eg, your name or your server\'s hostname) []:web.tp2.linux
[...]

[it4@front ~]$ sudo mv server.key /etc/pki/tls/private/web.tp2.linux.key
[it4@front ~]$ sudo mv server.crt /etc/pki/tls/certs/web.tp2.linux.crt

[it4@front ~]$ sudo chown root:root /etc/pki/tls/private/web.tp2.linux.key
[it4@front ~]$ sudo chown root:root /etc/pki/tls/certs/web.tp2.linux.crt
[it4@front ~]$ sudo chmod 400 /etc/pki/tls/private/web.tp2.linux.key
[it4@front ~]$ sudo chmod 644 /etc/pki/tls/certs/web.tp2.linux.crt
```

🌟 **Modifier la conf de NGINX**

```bash
# Conf du reverse proxy NGINX pour pointer vers NextCloud en HTTPS
[it4@front ~]$ sudo vim /etc/nginx/conf.d/web.tp2.linux.conf

[it4@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    listen                  443 ssl http2;
    server_name web.tp2.linux;

    # SSL
    ssl_certificate         /etc/pki/tls/certs/web.tp2.linux.crt;
    ssl_certificate_key     /etc/pki/tls/private/web.tp2.linux.key;


    location / {
        proxy_pass http://web.tp2.linux;
    }
}

# HTTP redirect
server {
    listen      80;
    server_name web.tp2.linux;

    location / {
        return 301 https://web.tp2.linux$request_uri;
    }
}

# Adaptation du firewall à la nouvelle conf
[it4@front ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[it4@front ~]$ sudo firewall-cmd --reload
```


🌟 **TEST**

```bash
[it4@nowhere ~]$ curl -k https://web.tp2.linux
<!DOCTYPE html>
<html>
<head>
	<script> window.location.href="index.php"; </script>
	<meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>
```

# IV. Firewalling


Il existe déjà une zone appelée `drop` qui permet de jeter tous les paquets. Il suffit d'ajouter nos interfaces dans cette zone.

sudo firewall-cmd --new-zone=ssh```bash
$ sudo firewall-cmd --list-all # on voit qu'on est par défaut dans la zone "public"
$ sudo firewall-cmd --set-default-zone=drop # on configure la zone "drop" comme zone par défaut
$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 # ajout explicite de l'interface host-only à la zone "drop"
```

**Ensuite**, on peut créer une nouvelle zone, qui autorisera le trafic lié à telle ou telle IP source :

```bash
$ sudo firewall-cmd --new-zone=ssh # le nom "ssh" est complètement arbitraire. C'est clean de faire une zone par service.
```

**Puis** on définit les règles visant à autoriser un trafic donné :

```bash
$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 # 10.102.1.1 sera l'IP autorisée
$ sudo firewall-cmd --zone=ssh --add-port=22/tcp # uniquement le trafic qui vient 10.102.1.1, à destination du port 22/tcp, sera autorisé
```

**Le comportement de FirewallD sera alors le suivant :**

- si l'IP source d'un paquet est `10.102.1.1`, il traitera le paquet comme étant dans la zone `ssh`
- si l'IP source est une autre IP, et que le paquet arrive par l'interface `enp0s8` alors le paquet sera géré par la zone `drop` (le paquet sera donc *dropped* et ne sera jamais traité)

> *L'utilisation de la notation `IP/32` permet de cibler une IP spécifique. Si on met le vrai masque `10.102.1.1/24` par exemple, on autorise TOUT le réseau `10.102.1.0/24`, et non pas un seul hôte. Ce `/32` c'est un truc qu'on voit souvent en réseau, pour faire référence à une IP unique.*

![Cut here to activate firewall :D](./pics/cut-here-to-activate-firewall-best-label-for-lan-cable.jpg)

## 2. Mise en place

### A. Base de données

🌞 **Restreindre l'accès à la base de données `db.tp2.linux`**

- seul le serveur Web doit pouvoir joindre la base de données sur le port 3306/tcp
- vous devez aussi autoriser votre accès SSH
- n'hésitez pas à multiplier les zones (une zone `ssh` et une zone `db` par exemple)

> Quand vous faites une connexion SSH, vous la faites sur l'interface Host-Only des VMs. Cette interface est branchée à un Switch qui porte le nom du Host-Only. Pour rappel, votre PC a aussi une interface branchée à ce Switch Host-Only.  
C'est depuis cette IP que la VM voit votre connexion. C'est cette IP que vous devez autoriser dans le firewall de votre VM pour SSH.

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

- `sudo firewall-cmd --get-active-zones`
- `sudo firewall-cmd --get-default-zone`
- `sudo firewall-cmd --list-all --zone=?`

### B. Serveur Web

🌞 **Restreindre l'accès au serveur Web `web.tp2.linux`**

- seul le reverse proxy `front.tp2.linux` doit accéder au serveur web sur le port 80
- n'oubliez pas votre accès SSH

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

### C. Serveur de backup

🌞 **Restreindre l'accès au serveur de backup `backup.tp2.linux`**

- seules les machines qui effectuent des backups doivent être autorisées à contacter le serveur de backup *via* NFS
- n'oubliez pas votre accès SSH

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

### D. Reverse Proxy

🌞 **Restreindre l'accès au reverse proxy `front.tp2.linux`**

```bash
[it4@front ~]$ sudo firewall-cmd --list-all
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: 
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
[it4@front ~]$ sudo firewall-cmd --list-all --zone=lan
lan (active)
  target: default
  icmp-block-inversion: no
  interfaces: 
  sources: 10.102.1.0/24
  services: 
  ports: 22/tcp 80/tcp 443/tcp
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```

### E. Tableau récap

🌞 **Rendez-moi le tableau suivant, correctement rempli :**

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?             |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | ?           | ?             |
