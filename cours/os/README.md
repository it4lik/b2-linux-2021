# Système d'exploitation

On va explorer un peu dans ce cours le concept d'OS au sens large.  

Il ne sera pas ici question de façon spécifique des OS GNU/Linux, sauf quand c'est mentionné.

# Sommaire 

- [Système d'exploitation](#système-dexploitation)
- [Sommaire](#sommaire)
- [I. La place de l'OS](#i-la-place-de-los)
- [II. Les constituants récurrents de l'OS](#ii-les-constituants-récurrents-de-los)
  - [A. Essentiels](#a-essentiels)
    - [1. Shell](#1-shell)
    - [2. Network stack](#2-network-stack)
    - [3. Devices handling](#3-devices-handling)
    - [4. Filesystem](#4-filesystem)
    - [5. Users](#5-users)
    - [6. Service manager](#6-service-manager)
  - [B. Annexes mais néanmoins nécessaires](#b-annexes-mais-néanmoins-nécessaires)
    - [1. Logs](#1-logs)
    - [2. Time](#2-time)
    - [3. Updates](#3-updates)
    - [4. Application Manager](#4-application-manager)
    - [5. Scheduling](#5-scheduling)

# I. La place de l'OS

On pourrait résumer les principaux composants d'une machine, d'un point de vue abstrait, à :

- **le matériel** (matériel)
- **le noyau** (logiciel)
- **l'OS** (logiciel)

**Le noyau et l'OS sont de simples applications**, comme n'importe quelle autre app. La différence entre le noyau, l'OS et d'autres applications plus communes, c'est juste leurs rôles respectifs au sein du système.

➜ **Le noyau est la seule entité qui sait communiquer avec tout le matériel.** C'est lui qui va, par exemple :

- dire aux disques d'écrires des 0 et des 1
- réceptionner les messages qu'envoie votre souris afin de faire bouger la souris à l'écran
- demander aux cartes réseau d'envoyer des messages sur le réseau

Bref, le noyau carry. De notre côté, on a besoin de l'OS afin de discuter avec le noyau.

➜ **L'OS a pour rôle de fournir une interfacement humainement utilisable vers le noyau.**

L'OS est donc le logiciel qui nous permet, par extension, de manipuler notre matériel, par le biais du noyau.

L'OS nous fournit aussi de nombreuses couches d'abstraction ce qui rend la machine humainement utilisable. Par exemple :

- on va gérer un *filesystem* (*système de fichiers*) et pas juste des 0 et des 1  
- on va gérer des *paquets* réseau et pas juste des 0 et des 1 dans des câbles
- un certain nombre de services nous seront automatiquement fournis, comme le fait d'avoir une horloge à jour

**On va voir dans ce cours un peu plus en détails les composants récurrents d'un OS. C'est à dire les différentes applications qui, ensemble, forment l'OS.**

> Par abus de langage, on dit que le noyau est dans l'OS. Il est vrai que le noyau est souvent livré en même temps que l'OS mais ce sont bien deux applications distinctes. Chacune a une fonction qui lui est propre.

**Rien n'est magique dans un OS**. La moindre chose qui se passe, il y a une ligne de code qui l'a causée.

# II. Les constituants récurrents de l'OS

La liste établie ici est arbitraire.

Ce que j'appelle *Essentiels* correspond aux éléments qu'on retrouve systématiquement et qui sont les piliers du système.  
A l'inverse, dans *Annexes*, on trouve des éléments moins cruciaux, mais souvent tout autant nécessaires.

> La liste n'est pas exhaustive. Elle a simplement pour but de vous éclaircir les idées sur la notion d'OS.

**🐧 Un emoji 🐧 figurera dans chaque partie pour vous indiquer comment s'appelle ce qu'on trouve sur GNU/Linux spécifiquement.**

## A. Essentiels

- [1. Shell](#1-shell)
- [2. Network stack](#2-network-stack)
- [3. Devices handling](#3-devices-handling)
- [4. Filesystem](#4-filesystem)
- [5. Users](#5-users)
- [6. Service manager](#6-service-manager)

### 1. Shell

**Un *shell* est une application qui permet à l'humain d'utiliser à la machine.** Il existe deux grands types de *shell* :

- ***CLI*** : interface en ligne de commande
  - *command-line interface*
- ***GUI*** : interface graphique
  - *graphical user interface*

L'une comme l'autre fournissent à l'utilisateur un ensemble d'outils pour discuter avec le noyau et par extension, le matériel.

---

🐧 Côté CLI on trouve beaucoup `bash`, sinon `sh` ou `zsh` (non-exhaustif).

🐧 Côté GUI : [XFCE](https://www.xfce.org/), [KDE](https://kde.org/fr/), [GNOME](https://kde.org/fr/), [LXDE](http://www.lxde.org/), [Mate](https://mate-desktop.org/fr/) (non-exhaustif)

### 2. Network stack

La *Network stack* est aussi appelée *TCP/IP stack*, *pile réseau* ou *pile TCP/IP* ou encore *stack réseau*.

**La *stack réseau* c'est l'ensemble des applications qui permettent de gérer les cartes réseau.** Par exemple :

- définir des adresses IP sur des cartes réseau
- effectuer les requêtes ARP automatiquement
- construction de paquets IP
- résolution DNS
- requêtes DHCP
- etc etc etc...

---

🐧 Sur les bases Debian, on gère la conf réseau élémentaire dans `/etc/network/interfaces`. Côté RedHat c'est le dossier `/etc/sysconfig/network-scripts/`.  
🐧 Il existe aussi désormais `NetworkManager` qui permet de faciliter la gestion de ces fichiers (notamment en se connectant facilement aux réseaux WiFi depuis une interface graphique par exemple).

### 3. Devices handling

**La gestion des *périphériques* (*devices* en anglais) est essentielle dans le système.**

L'OS nous permet de gérer les *périphériques* branchés à une machine.  
On entend par *périphérique* tous les machins qu'on va brancher à une machine, en plus de ses composants essentiels.

Souris, écran, disque dur additionnel, carte son, carte réseau, etc etc. L'OS nous permet d'interagir avec eux.

> Dans Linux, la plupart des périphériques sont accessibles dans `/dev/`.

### 4. Filesystem

**Un *filesystem* ou *fs* ou *système de fichiers* est un outil permettant de lire et écrire sur un périphérique de stockage avec des notions facilements manipulables pour les humains.**  
Exit les 0 et les 1. En tant qu'humains, on manipule des fichiers et des dossiers.

C'est l'OS qui nous permet de gérer nos partitions, et d'y créer des systèmes de fichiers.  
C'est précisément le système de fichiers qui rend intelligible le contenu d'un disque dur.

Un *fs* c'est juste un truc qui range votre disque comme un gros tableau.  
Il stocke, **pour chaque fichier**, entre autres :

- l'endroit sur le disque où le fichier commence
- l'endroit sur le disque où le fichier termine
- le nom du fichier

---

🐧 Le filesystem le plus utilisé sur les OS GNU/Linux est `ext4`. D'autres existent, proposant des fonctionnalités alternatives.

### 5. Users

**La gestion d'utilisateurs est, dans les systèmes modernes, un des éléments cruciaux de la sécurité de nos OS.**

**Qui dit gestion d'utilisateurs dit aussi gestion de permissions.**

Grâce à l'OS on va pouvoir créer des utilisateurs, et leur donner des permissions sur les pans du système qu'ils auront besoin d'utiliser. 

De façon plus concrète, on parle principalement des droits que les utilisateurs auront sur le *fs* :

- en lecture
- en écriture
- en exécution

> On garde toujours à l'esprit le ***principe du moindre privilège*** ou *least privilege principle* qui est une bonne pratique visant à n'attribuer que le strict minimum nécessaire à chaque utilisateur en terme de permissions. 

---

➜ **Sur les OS GNU/Linux, on dit que "tout est fichier".** C'est une façon courte de dire que vous pouvez accéder à tout comme si c'était un simple fichier du fs : périphérique, fichiers de configuration des applications, même la gestion des utilisateurs c'est dans des fichiers. Des fichiers texte.

**Ainsi, un utilisateur avec beaucoup de droits (permissions lecture/écriture/exécution) pourra complètement modifier le système à sa guise.**  

A l'inverse, un utilisateur qui n'a que peu de permissions n'aura qu'un impact extrêmement limité sur le système.

➜ **Ce que j'essaie de vous dire c'est que la gestion des droits `rwx` sur un système de GNU/Linux c'est le COEUR de la sécurité. C'est élémentaire, d'une grande simplicité et surtout d'une redoutable efficacité.**

> Un OS où les permissions `rwx` sont bien gérées, c'est un OS qui se prémunit de 95% des attaques.

### 6. Service manager

**Un autre rôle de l'OS est de gérer des *services*. Un *service* c'est juste une application dont l'OS s'occupe.**

Plutôt qu'un humain lance tous les programmes à la main, ce qui est chiant, le système va s'occuper de beaucoup d'entre eux.  
Toutes les applications nécessaires pour avoir un OS complet, autonome, sont packagées sous forme de *service* et l'OS a pour charge de les maintenir en bonne santé.

> Vous utilisez le bluetooth sur votre machine ? Alors il y existe un service, qui tourne, en permanence, et qui a pour charge de traiter ce qui est relatif au bluetooth.

**Un autre avantage de l'utilisation de services plutôt que de lancer les apps à la main, c'est qu'on va pouvoir définir de façon consistante l'environnement d'exécution de l'application.**

Définissons les termes :

- **de façon consistante**
  - on définit ça dans un ou plusieurs fichiers (les `.service` dans un système GNU/Linux)
  - c'est clean
  - c'est déclaré qu'une seule fois : une bonne fois pour toute
- **environnement d'exécution**
  - c'est tous les éléments qui sont autour de l'application
  - par exemple, on peut définir :
    - un répertoire depuis lequel doit se lancer l'app
    - un utilisateur avec lequel doit se lancer l'app
    - des paramètres de sécurité

C'est juste très pratique, et ça permet d'avoir un OS très organisé, dont la composition est claire, lisible et maintenable pour un humain.

---

🐧 C'est [*systemd*](../systemd/README.md) qui a cette charge dans la plupart des OS GNU/Linux

## B. Annexes mais néanmoins nécessaires

- [1. Logs](#1-logs)
- [2. Time](#2-time)
- [3. Updates](#3-updates)
- [4. Application Manager](#4-application-manager)
- [5. Scheduling](#5-scheduling)

### 1. Logs

**Les *journaux* ou *logs* sont des simples fichiers texte que génèrent les applications pendant leur fonctionnement.**  
Les applications inscrivent dans leurs *journaux* des informations dignes d'intérêt pour les humains.

On les consulte généralement lorsqu'une application dysfonctionne, afin de voir d'où vient le soucis.

L'OS va gérer les logs :

- l'OS nous permet, en tant qu'utilisateur humain, de consulter les logs
- il peut les centraliser afin d'en faciliter la consultation, le traitement
- il va gérer leur taille, afin que les logs du système ne remplissent pas le stockage de la machine

**On distingue plusieurs niveaux de logs,** suivant la gravité de l'évènement.  
Il existe des standards pour ça, mais je vais me contenter de lister les trucs qu'on voit partout :

- **`error`** : bon bah, erreur quoi. Pas cool. Le programme va dysfonctionner, voire s'arrêter.
- **`warning`** : un simple avertissement. Il ne va pas perturber le fonctionnement du programme. Cela peut être un avertissement qui pourrait se transformer en `error` un jour, mais ça peut aussi être insignifiant.
- **`notice` ou `info`** : le programme nous informe de tout ce qu'il fait. C'est souvent quand on active le mode verbeux des logs qu'un programme génère ce genre de logs.

![forgot ur password ?](./pics/securitylogs.jpg)

### 2. Time

**Nos systèmes d'exploitation doivent rester à l'heure, et synchronisés les uns avec les autres**, pour de multiples raisons, entre autres : 

- les *logs* sont munis d'un *timestamp* : ils sont datés avec l'heure et le jour
  - il faut qu'ils soient datés correctement pour corréler les évènements entre deux machines
  - les différentes machines
- certaines applications permettant la *redondance* se basent sur l'heure pour fonctionner
  - genre deux routeurs, avec un qui prend le relai si l'autre est en panne, ils utilisent l'heure pour se synchroniser

> La gestion de l'heure n'est pas un problème trivial. Son impact peut être énorme lors d'un dysfonctionnement, et il est loin d'être simple de calculer la durée d'une seconde à la perfection (c'est impossible en fait, même nos meilleures horloges atomiques se plantent un peu...).

> Petit point culture G gratuit, la seconde est une unité élémentaire du système international, et elle est définie par la durée de 9 192 631 770 oscillations d'un atome de césium, et *spoiler alert* votre PC ne compte pas les oscillations d'un atome de césium en permanence hihi.

Il existe deux principaux moyens pour qu'une machine dispose de l'heure :

- **hardware clock**
  - c'est votre processeur qui contient une horloge
  - l'horloge émet des *ticks*
  - à chaque *tick* qui passe, une seconde s'est écoulée
  - l'OS peut accéder à cette horloge
- **le protocole *NTP***
  - c'est pour *Network Time Protocol*
  - c'est un protocole qui permet à une machine de demander l'heure sur le réseau
    - il existe des *serveurs NTP*, à qui on peut demander l'heure
    - tout le monde peut être un *client NTP* afin de demander à un *serveur NTP* l'heure qu'il est, à travers le réseau

> Tous nos laptops modernes sont automatiquement à l'heure. C'est grâce au protocole *NTP* :)

**Dans un cas réel, lorsqu'on gère un parc de machines :**

- on install et configure un *serveur NTP* dans notre infra
- on demande à *toutes* les autres machines du parc de se synchroniser sur notre *serveur NTP*
- on demande à notre *serveur NTP*, quant à lui, de se synchroniser sur un *serveur NTP* externe, de référence

> Il existe le projet [pool.ntp.org](https://www.pool.ntp.org/fr/) qui a pour objectif de fournir des *serveurs NTP* libres d'accès pour tous, sur internet.

**La synchronisation des machines entre elles au niveau de l'heure est un problème tout aussi épineux que la gestion locale de l'heure au sein d'une unique machine.**  
Son impact peut lui aussi être énorme. [Random article qui parle d'un bug assez grave lié à la leap second](https://www.wired.com/2012/07/leap-second-bug-wreaks-havoc-with-java-linux/).

### 3. Updates

Ayayaya, les mises à jour. Votre OS gère les mises à jour en tout genre. Principalement :

- les mises à jour de... bah de l'OS hein
- les mises à jour des services et des autres applications du système

La plupart des patchs qu'on reçoit, sur l'OS comme sur les services ne sont pas des nouvelles features mais plutôt...

- des patchs de sécurité
- des corrections de bug
- améliorations des performances

### 4. Application Manager

On parle ici d'un outil qui permet d'ajouter, de supprimer et de mettre à jour des applications sur le système.  
**Il se présente généralement sous la forme d'un *gestionnaire de paquets*.**

Un outil pour gérer des applications n'est pas tout le temps incorporé aux OS :

- les distributions GNU/Linux
  - il y a toujours un *gestionnaire de paquets*
  - c'est généralement ce *gestionnaire de paquets* qui se charge aussi des mises à jour du système, en plus des autres applications
- MacOS n'en possède pas par défaut
  - mais vous pouvez use [brew](https://brew.sh/index_fr)
- Microsoft Windows
  - le très bon, opensource et libre [Chocolatey](https://chocolatey.org/)
  - Microsoft, après 30 ans d'hibernation semble avoir *enfin* développé un gestionnaire de paquets natif : [winget](https://github.com/microsoft/winget-cli)

### 5. Scheduling

**La planification de tâches ou *tasks scheduling* désigne la pratique consistant à exécuter des programmes à intervalles réguliers.**

Ceci est un mécanisme essentiel au bon fonctionnement d'un OS. C'est grâce à des tâches planifiées que le système va pouvoir par exemple clean des dossiers temporaires ou bien de gérer la taille des logs système.

C'est aussi une fonctionnalité très utilisée par les administrateurs humains.
