# systemd et processus

***systemd*** est le nom d'une application qui gère une grosse partie de la partie [*OS*](../os/README.md) sur les systèmes GNU/Linux.

On va aussi en profiter pour s'intéresser de plus près à **la notion de *processus*.**

![systemd thing](../systemd/pics/systemd-thing.png)

# Sommaire

- [systemd et processus](#systemd-et-processus)
- [Sommaire](#sommaire)
- [I. systemd c koa](#i-systemd-c-koa)
- [II. PID 1](#ii-pid-1)
  - [1. Notions générales autour des processus](#1-notions-générales-autour-des-processus)
    - [A. Qu'est-ce qu'un processus ?](#a-quest-ce-quun-processus-)
    - [B. Caractéristiques d'un processus](#b-caractéristiques-dun-processus)
    - [C. Les signaux](#c-les-signaux)
      - [➜ `SIGTERM` 15, et `SIGINT` 2](#-sigterm-15-et-sigint-2)
      - [➜ `SIGKILL` 9](#-sigkill-9)
      - [➜ `SIGHUP` 1](#-sighup-1)
      - [➜ La commande `kill`](#-la-commande-kill)
    - [D. Notion de PID 1](#d-notion-de-pid-1)
  - [2. systemd en tant que PID 1](#2-systemd-en-tant-que-pid-1)
- [III. Gestion de services](#iii-gestion-de-services)
  - [1. Unité systemd](#1-unité-systemd)
  - [2. Contenu d'une unité de type service](#2-contenu-dune-unité-de-type-service)
  - [3. Interagir avec une unité](#3-interagir-avec-une-unité)
- [IV. Autres fonctions de systemd](#iv-autres-fonctions-de-systemd)
  - [1. journald](#1-journald)
- [V. Controverse *systemd*](#v-controverse-systemd)

# I. systemd c koa

➜ ***systemd* est un outil qui gère beaucoup de facettes du [système d'exploitation](../os/README.md)**, notamment (la liste n'est pas exhaustive) :

- [gestion de services](../os/README.md#6-service-manager)
- [gestion de l'heure](../os/README.md#2-time)
- [gestion du réseau](../os/README.md#2-network-stack)
- [planification de tâches](../os/README.md#5-scheduling)

➜ *systemd* a été très largement adopté dans le monde GNU/Linux. En effet, toutes les distributions les plus utilisées l'ont inclus dans leur OS. Liste non-exhaustive :

- RedHat (RHEL) et dérivés (CentOS, Fedora, Rocky, etc.)
- Debian et dérivés (Ubuntu, Mint, Kali, etc.)
- ArchLinux

➜ ***systemd* a aussi le rôle de *PID 1*** chose que nous allons discuter dans une partie dédiée. Il a donc un rôle prépondérant lors du démarrage du système, mais aussi pendant que le système est allumé.

➜ Pour endosser ces rôles correctement, *systemd* est capable d'interagir nativement avec le noyau, sur beaucoup d'aspects. Ainsi, beaucoup de choses que l'on réalisait avant *via* des scripts shell sont désormais réalisables avec *systemd*. Par exemple :

- écoute sur un port
- montage de partitions
- création de [*services*](../os/README.md#6-service-manager)
- communication entre les processus
- sécurité des applications

Dans les parties qui suivent, on va rentrer un peu plus deep dans les fonctions de *systemd*.

![all getting distracted :3](pics/linux-distros-a-simple-decent-nt-system-systemd-theyre-all-27321737.png)

# II. PID 1

## 1. Notions générales autour des processus

Ces notions générales autours des processus sont valables pour tous les OS modernes GNU/Linux, mais aussi Windows ou MacOS.

### A. Qu'est-ce qu'un processus ?

➜ **Un *processus* est un *programme* en cours d'*exécution*.**

➜ Un ***programme*** ou encore *application*, *logiciel*, etc. **est un fichier que l'on peut exécuter**. Qu'il contienne du code compilé ou du code interprété, du moment qu'on peut l'exécuter, c'est un *programme*.

> Un `.exe` est un *programme*, un script `bash` ou `python` aussi.

Un *programme* est donc un simplie fichier sur le disque.

➜ **"*Exécuter*" un *programme* c'est le déplacer en RAM, et demander au CPU d'exécuter les instructions qu'il contient.**

Une fois que le *programme* est en cours d'exécution dans la RAM, on l'appelle *processus*.

### B. Caractéristiques d'un processus

➜ **Un *processus* est géré par le noyau.** C'est lui qui entretient la liste des processus, et fait en sorte qu'elle soit cohérente.

➜ **Il existe une notion de parenté entre les processus.**

Un *processus* est TOUJOURS créé par un autre *processus*. On parle alors de ***processus* *parent*** et ***processus* *enfant***. C bo putain.

Si un processus se fait abandonner, on parle de ***processus* *orphelin*** (*orphan process*).

> Quand vous lancez votre navigateur Web préféré, genre Firefox, vous le faites en cliquant sur un machin dans votre interface graphique, votre [GUI](../os/README.md#1-shell). Votre GUI est un *processus*. Firefox sera l'enfant de votre GUI.

![killing a child](../systemd/pics/killing-a-child.jpg)

➜ **Caractéristiques d'un *processus*** :

- **possède un *PID***
  - c'est son identifiant unique, c'est un entier
  - attribué par le noyau
- **possède un *PPID***
  - c'est le *PID* de son père
  - cela permet de savoir qui est l'enfant de qui
- **s'exécute dans un *environnement d'exécution*** ou (*runtime*)
  - on désigne par ce terme abstrait les caractéristiques du *programme* lors de son lancement
  - par exemple :
    - un utilisateur qui va lancer le *programme* : ce sera l'identité sous laquelle tourne le *processus*
    - un dossier spécifique depuis lequel le *programme* doit être exécuté

### C. Les signaux

Un *signal* est un message simple qu'on peut envoyer à un *processus* en cours d'*exécution*.

Sur GNU/Linux, vous pouvez consulter la liste des signaux qu'on peut envoyer à un processus dans [le manuel de `signal` (`man signal`)](https://man7.org/linux/man-pages/man7/signal.7.html).

Certains signaux sont envoyés au *processus* ciblé, libre à lui de les intrerpréter comme il veut.

D'autres sont envoyés au *noyau* pour qu'il agisse sur le *processus*. Le *noyau* étant le coeur de la machine, il fait ce qu'il veut, il peut éclater des *process* tranquillement :)

**On va voir certains des signaux les plus utilisés sous GNU/Linux.**

#### ➜ `SIGTERM` 15, et `SIGINT` 2

*"Coucou petit process, STP ferme-toi proprement".*

- *signal* envoyé à un *processus* en cours d'*exécution*
- c'est pour demander gentilment au process de se terminer
- à l'intérieur du programme il existe une fonction qui est appelée lorsqu'il reçoit l'un de ces *signaux* : c'est la procédure pour quitter proprement. Elle contient souvent :
  - tuer ses processus enfants
  - fermer les fichiers ouverts pour écrire ou lire dedans
  - fermer les connexions réseau
- le développeur peut aussi choisir d'ignorer ce genre de *signaux* dans son code

> C'est ce qui est envoyé à un *programme* **quand vous cliquez sur la croix rouge pour le fermer** par exemple. Ou encore **quand vous appuyez sur `CTRL + C`** dans un terminal pour stopper l'exécution d'un truc.

#### ➜ `SIGKILL` 9

*"Bonjour noyau, tu peux éclater le processus qui porte l'identifiant X stp ?"*

- *signal* envoyé au *noyau*
- le *noyau* va couper le *processus* : il va enlever l'espace RAM qu'il avait attribué au *processus*
- ainsi, le *processus* n'a plus d'espace en RAM où s'exécuter : il meurt forcément
- le *processus* n'a pas son mot à dire donc il quitte de façon crade
  - ses *processus enfants*, s'il en a, deviennent *orphelins*
  - les fichiers restent ouverts, parfois deviennent impossible à modifier
  - les connexions réseau tombent salement

#### ➜ `SIGHUP` 1

*"Hello processus, recharge ta configuration STP."*

- *signal* envoyé à un *processus* en cours d'*exécution*
- demande au *programme* de reload sa conf
- il faut que le *programme* sache comment interpréter ce *signal* sinon il n'aura aucun effet

#### ➜ La commande `kill`

La commande `kill` permet d'envoyer des signaux à des process :

```bash
$ kill -<SIGNAL> <PID>

# Par exemple pour envoyer un SIGINT au processus firefox
## On repère l'ID du process firefox
$ ps -ef | grep firefox

## On envoie le signal le process en connaissant son ID
$ kill -2 7256
```

> **La commande `kill -9` a mauvaise réputation** car elle permet d'envoyer le `SIGKILL`. Ainsi, les *processus* tués de cette façon laisseront souvent des *reliquats* derrière eux : des fichiers ouverts qui auraient du être supprimés à la fermeture du *programme* par exemple.

![i will find you](pivs/../pics/i-will-find-you-and-kill-9-all-of-your-processes.jpg)

### D. Notion de PID 1

**Le *PID 1* est le premier processus lancé par le *noyau*, lorsque la machine s'allume ; ainsi, il porte l'identifiant 1.** 

**On l'appelle aussi souvent *init*.**

Nous avons vu deux de ses fonctions en cours :

- **lance d'autres *processus***
  - ce sont les *processus système*
  - ceux qui, ensemble, forment le [système d'exploitation](../os/README.md)
- **adopte les [*processus orphelins*](#b-caractéristiques-dun-processus)**
  - et oui, il leur faut bien un papa hihi
  - par exemple, quand leur père a reçu un `kill -9` entre les deux yeux, c'est moche

## 2. systemd en tant que PID 1

*systemd* occupe donc cette fonction de *PID 1* au sein des systèmes GNU/Linux : c'est lui qui fait démarrer, qui fait *boot* l'OS.

Ainsi, il gère toute la chaîne de *boot* de l'OS grâce à des *services*, dont il peut paralléliser le lancement.

> Cette parallélisation a fait gagner ~60% de temps de boot aux machines GNU/Linux lors du passage à *systemd*.

Ainsi, c'est lui qui, au démarrage, donne notamment l'ordre de :

- monter les partitions
- configurer et allumer les cartes réseaux
- lancer un [*shell*](../os/README.md#1-shell) pour qu'un utilisateur puisse se connecter (interface graphique : GUI ou terminal : CLI)
- lancer les programmes au démarrage demandés par l'utilisateur

# III. Gestion de services

Cette notion est abordée d'un point de vue théorique dans [le cours dédié aux OS de façon générale](../os/README.md#6-service-manager). **Sa lecture est un prérequis.**

---

*systemd* gère les *services* au sein des systèmes GNU/Linux. Que ce soit la plupart des processus système, ou des *services* ajoutés par les utilisateurs, c'est lui qui en est charge.

## 1. Unité systemd

Une *unité systemd* est un fichier qui définit une entité que *systemd* peut gérer. Elle peut être de plusieurs *types* :

- *`.service`*
  - c'est ce qui nous intéresse le plus dans cette partie
  - cela sert à lancer un *programme* dans un *environnement* spécifique
- *`.timer`*
  - exécution de *services* de façon périodique
  - remplacement de `cron` qui est nativement lié aux *services*
- *`.target`*
  - c'est un ensemble d'autres *unités systemd*
  - simplifie la gestion
  - les OS GNU/Linux modernes sont paramétrés pour que *systemd*, une fois lancé en tant que *[PID 1](#d-notion-de-pid-1*, lance le `multi-user.target`

> **C'est l'extension du fichier qui détermine le *type* de l'*unité systemd*.**

## 2. Contenu d'une unité de type service

➜ **Cas d'école :**

```bash
# Les deux commandes sont équivalentes. Lorsqu'on précise pas le type d'unité, systemd ajoute .service par défaut
$ systemctl cat test
$ systemctl cat test.service
[Unit]
Description=Unité de test
After=network.target

[Service]
ExecStart=/bin/sleep 9999

[Install]
WantedBy=multi-user.target
```

➜ **Version commentée :**

```bash
# Section Unit, obligatoire dans toutes les unités systemd, peu importe leur type
[Unit]
# Description arbitraire
Description=Unité de test
# On peut aussi préciser des relations de dépendances avec d'autres unités dans cette section
After=network.target

# On déclare une section qui porte le nom correspondant au type de l'unité
# Ici un .service
[Service]
# ExecStart est la clause la plus important dans un .service
# c'est la commande qui est lancé quand on démarre le .service 
ExecStart=/bin/sleep 9999

# Section optionnelle, qui permet surtout de définir dans quel .target est contenu l'unité
[Install]
# Le multi-user.target est lancé au démarrage de la machine, ainsi, notre service le sera aussi si on l'enable
WantedBy=multi-user.target
```

## 3. Interagir avec une unité

On interagit avec les unités à l'aide de la commande `systemctl`.

| Commande                      | Action                                                                                                      | Commentaire                                                                                                                                                                        |
|-------------------------------|-------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `systemctl status <UNIT>`     | Affiche l'état d'un service : s'il est démarré, enabled, ses derniers logs, l'ID de ses processus, etc.     | Premier réflexe à avoir pour connaître l'état d'un *service*.                                                                                                                      |
| `systemctl start <UNIT>`      | Démarre un service                                                                                          |                                                                                                                                                                                    |
| `systemctl stop <UNIT>`       | Stoppe un service                                                                                           |                                                                                                                                                                                    |
| `systemctl restart <UNIT>`    | Redémarre un service                                                                                        |                                                                                                                                                                                    |
| `systemctl reload <UNIT>`     | Demande à un *service* de recharger sa conf                                                                 | Il faut que le *programme* le supporte. On préfère souvent `restart`.                                                                                                              |
| `systemctl enable <UNIT>`     | Le service démarrera en même tant que le `target` dans lequel il est.                                       | Le `multi-user.target` étant lancé au démarrage de la machine, le fait d'`enable` un *service* qui est dans le `multi-user.target` revient à le lancer au démarrage de la machine. |
| `systemctl is-enabled <UNIT>` | Détermine simplement si       un service a été `enable`                                                     |                                                                                                                                                                                    |
| `systemctl is-active <UNIT>`  | Détermine simplement si  un service est en cours d'exécution                                                |                                                                                                                                                                                    |
| `systemctl cat <UNIT>`        | Affiche le fichier dans lequel est défini une *unité systemd*, ainsi que le chemin où se trouve ce fichier. |                                                                                                                                                                                    |

# IV. Autres fonctions de systemd

*systemd* occupe d'autres fonctions dans le système dont voici quelques exemples.

## 1. journald

***journald* est le nom d'un *programme* livré avec *systemd* qui va [gérer les logs](../os/README.md#1-logs) des *unités systemd*.**

On interagit avec lui à l'aide de la commande `journalctl`.

| Commande                   | Action                                | Commentaire                                                            |
|----------------------------|---------------------------------------|------------------------------------------------------------------------|
| `journalctl -xe`           | Affiche les logs de toutes les unités |                                                                        |
| `journalctl -xe -u <UNIT>` | Affiche les logs d'une unité ciblée   | On peut cumuler les `-u` par exemple `journalctl -xe -u sshd -u httpd` |
| `journalctl -xe --since yesterday` | Affiche les logs depuis une date précise   | On peut préciser des heures et jour précis |

Il existe toujours les fichiers dans `/var/log/` qui contiennent les fichiers de logs des applications. *journald* permet simplement d'en faciliter la gestion et de récupérer d'autres logs, qui étaient difficilement accessibles avant.

Le fait de pouvoir consulter les logs de toute la machine depuis un endroit unique est un outil extrêmement pratique pour tout admin.

# V. Controverse *systemd*

L'adoption de *systemd* sur la plupart des grandes distributions GNU/Linux a été très conversée, car le programme en lui-même, dans la façon dont il a été conçue, laisse la porte ouverte à des reproches parfaitement fondés.

Les arguments pour *systemd* :

- une façon unique et **standard** de gérer un OS GNU/Linux
  - la syntaxe des unités est standard
  - même si on bascule d'un Linux à un autre
- **un environnement clair et maintenable**
  - tout le système est constitué d'unité
  - la syntaxe est appréhensible facilement par un humain
  - avant, c'était des scripts shell
- des interactions fortes entre ses composants
  - tous les composants de *systemd* interagissent nativement et facilement
  - par exemple :
    - relation de dépendances entre les *unités systemd*
    - communication entre les services simplifiée
    - isolation et sécurité

Les arguments contre *systemd* :

- **réinvente la roue**
  - on avait déjà avant *systemd* des programmes pour faire tout ce qu'il fait
  - inutile de remplacer `cron` par les `.timer` par exemple
  - ni de même de remplacer ce bon vieux `init` de *sysV* (ancêtre de *systemd*) par celui de *systemd*
- **monolithique**
  - c'est un seul gros bloc de code unique
    - plutôt faux, *systemd* est au contraire modulaire, mais vous pouvez entendre cet argument
  - c'est un *SPOF* : un point unique de défaillance
    - si *systemd* plante, le système dans son ensemble est au sol

![systemd eat all the thiiings](pics/systemd-eat-all-the-things.gif)