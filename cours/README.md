# Cours Linux B2

**Cours**

- [FHS : File Hierarchy Standard](./fhs/README.md)
- [Partitionnement](./part/README.md)
- [Clés SSH](./ssh/README.md)
- [Systèmes d'exploitation](./os/README.md)
- [systemd et processus](./systemd/README.md)

**Mémos**

- [Mémo commandes](./memo/commandes.md)
- [Mémo réseau Rocky Linux](./memo/rocky_network.md)
- [Etapes installation de la VM patron Rocky Linux](./memo/install_vm.md)

**Bonus**

- [Intro Crypto](./bonus/intro_crypto/README.md)
  - Encodage
  - Hashing
  - Chiffrement
    - Symétrique
    - Asymétrique
  - Applications du chiffrement asymétrique
    - confidentialité
    - signature
- [Intro Crypto 2](./bonus/intro_crypto_2/README.md)
  - RSA en détails
  - TLS
  - Certificats
- [Anonymat en ligne](./bonus/tor/README.md)
  - Démystification Deepweb/Darkweb
  - Tor
  - VPN
  - Comparatif
- [Docker](./bonus/docker/README.md)
  - présentation de Docker
  - cas d'utilisation
  - quelques exemples de commandes