# B2 Linux 2021

Vous trouverez ici les ressources liées au cours autour de GNU/Linux de deuxième année.

**[Cours](./cours/README.md)**

- [FHS : File Hierarchy Standard](./cours/fhs/README.md)
- [Partitionnement](./cours/part/README.md)
- [Clés SSH](./cours/ssh/README.md)
- [Systèmes d'exploitation](./cours/os/README.md)
- [systemd et processus](./cours/systemd/README.md)

**Mémos**

- [Mémo commandes](./cours/memo/commandes.md)
- [Mémo réseau Rocky Linux](./cours/memo/rocky_network.md)
- [Etapes installation de la VM patron Rocky Linux](./cours/memo/install_vm.md)

**Cours Bonus**

- [Intro Crypto](./cours/bonus/intro_crypto/README.md)
  - Encodage
  - Hashing
  - Chiffrement
    - Symétrique
    - Asymétrique
  - Applications du chiffrement asymétrique
    - confidentialité
    - signature
- [Intro Crypto 2](./cours/bonus/intro_crypto_2/README.md)
  - RSA en détails
  - TLS
  - Certificats
- [Anonymat en ligne](./cours/bonus/tor/README.md)
  - Démystification Deepweb/Darkweb
  - Tor
  - VPN
  - Comparatif
- [Docker](./cours/bonus/docker/README.md)
  - présentation de Docker
  - cas d'utilisation
  - quelques exemples de commandes